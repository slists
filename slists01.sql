--
-- Create a very simple database to hold lists information
--
PRAGMA foreign_keys = ON;

CREATE TABLE users (
  id            INTEGER PRIMARY KEY,
  username      TEXT,
  password      TEXT,
  email_address TEXT,
  first_name    TEXT,
  last_name     TEXT,
  active        INTEGER
);

create table lists (
 id INTEGER PRIMARY KEY,
 user_id integer,
 title text,
 FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE
);

create table list_items (
 id INTEGER PRIMARY KEY,
 product_quantity INTEGER,
 product_id INTEGER,
 list_id int,
 FOREIGN KEY (product_id) REFERENCES products(id) ON UPDATE CASCADE,
 FOREIGN KEY (list_id) REFERENCES lists(id) ON UPDATE CASCADE
);

create table products (
 id INTEGER PRIMARY KEY,
 name TEXT,
 weight REAL, --kg
 cost REAL --XXX rename to price $
);
 
--
-- Write two users, one of them (#1) has a list that contains
--  4kg potatoes cost $5
--
insert into users values (NULL, 'user1','user1','user1@localhost',
                          'userf','userl',1);
insert into users values (NULL, 'user2','user2','user2@localhost',
                          'userf2','userl2',1);
insert into users values (NULL, 'user3','user3','user3@localhost',
                          'userf3','userl3',1);
insert into lists values (NULL, 1, "A first list");
insert into products values(NULL, 'potatoes', 4, 5);
insert into list_items values (NULL, 1, 1,1); -- insert 1 product #1 into list 1

ALTER TABLE lists ADD created TIMESTAMP;
ALTER TABLE lists ADD updated TIMESTAMP;
UPDATE lists SET created = DATETIME('NOW'), updated = DATETIME('NOW');
SELECT * FROM lists;
