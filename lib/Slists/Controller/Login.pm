package Slists::Controller::Login;
use Moose;
use namespace::autoclean;
use Slists::Form::User;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

Slists::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

Login logic

=cut


sub index :Path :Args(0) {
    my ($self, $c) = @_;
    
    if ($c->request->method eq "POST"){
        # Get the username and password from form
        my $username = $c->request->params->{username};
        my $password = $c->request->params->{password};

        # If the username and password values were found in form
        if ($username && $password) {
            # Attempt to log the user in
            if ($c->authenticate({ username => $username,
                                   password => $password  } )) {
                # If successful, then let them use the application
                $c->response->redirect($c->uri_for(
                    $c->controller('Lists')->action_for('list')));
                return;
            } else {
                # Set an error message
                $c->stash(error_msg => "Bad username or password.");
            }
        } else {
            # Set an error message
            $c->stash(error_msg => "Empty username or password.")
                unless ($c->user_exists);
        }
    }
    # If either of above don't work out, send to the login page
    $c->stash(template => 'login.tt2');
}

=head2 create

Use HTML::FormHandler to create a new list

=cut

sub register :Path('/register') :Args(0) {
    my ($self, $c ) = @_;
    my $user = $c->model('DB::User')->new_result({});
    $c->stash( my_status_msg => "Registration successful" );
    return $self->form($c, $user);
}

=head2 form

Process the FormHandler list form
This is a separate function, because it's used by both /create and /edit.

=cut

sub form{
    my ( $self, $c, $user ) = @_;
    my $form = Slists::Form::User->new;
    # Set the template
    $c->stash( template => 'user.tt2', form => $form );
    $form->process(
      item => $user,
      params => $c->req->params,
    );
    return unless $form->validated;
    # Set a status message for the user & return to books list
    return $c->response->redirect($c->uri_for($self->action_for('login'),
        {mid => $c->set_status_msg($c->stash->{my_status_msg})}));
}

=head1 AUTHOR

user,,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
