package Slists::Controller::Lists;
use Moose;
use namespace::autoclean;
use Data::Dumper; #XXX
use Slists::Form::List;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

Slists::Controller::Lists - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched Slists::Controller::Lists in Lists.');
}



=head2 base

Can place common logic to start chained dispatch here

=cut

sub base :Chained('/') :PathPart('lists') :CaptureArgs(0) {
    my ($self, $c) = @_;

    # Store the ResultSet in stash so it's available for other methods
    $c->stash(resultset => $c->model('DB::List'));

    # Print a message to the debug log
    $c->log->debug('*** INSIDE BASE METHOD ***');
    
    # Load status messages
    $c->load_status_msgs;
}

=head2 list

Fetch all lists objects [for this user] and pass to lists/list.tt2 in stash to be displayed

=cut

sub list :Chained('base') :PathParth('list') :Args(0) {
    # Retrieve the usual Perl OO '$self' for this object. $c is the Catalyst
    # 'Context' that's used to 'glue together' the various components
    # that make up the application
    my ($self, $c) = @_;
    
    # retrieve all lists for this user
    $c->stash(lists => [$c->model('DB::List')->search({
        user_id => $c->user->id,
      })->all
    ]);
    
    # Set the TT template to use.  You will almost always want to do this
    # in your action methods (action methods respond to user input in
    # your controllers).
    $c->stash(template => 'lists/list.tt2');
}

=head2 object

Fetch the specified list object based on the list ID and store
it in the stash

=cut

sub object :Chained('base') :PathPart('id') :CaptureArgs(1) {
    # $id = primary key of book to delete
    my ($self, $c, $id) = @_;

    # Find the book object and store it in the stash
    $c->stash(object => $c->stash->{resultset}->find($id));

    # Make sure the lookup was successful.  You would probably
    # want to do something like this in a real app:
    #   $c->detach('/error_404') if !$c->stash->{object};
    $c->log->debug("Lists::object - List id is $id");
    die "List $id not found!" if !$c->stash->{object};

    # Print a message to the debug log
    $c->log->debug("*** INSIDE OBJECT METHOD for obj id=$id ***");
}

=head2 delete

Delete a list

=cut

sub delete :Chained('object') :PathPart('delete') :Args(0) {
    my ($self, $c) = @_;
    
    # Saved the PK id for status_msg below
    my $id = $c->stash->{object}->id;

    # Use the list object saved by 'object' and delete it [along
    # with related 'list_author' entries]
    $c->stash->{object}->delete;
    
    # Redirect the user back to the list page
    $c->response->redirect($c->uri_for($self->action_for('list'),
        {mid => $c->set_status_msg("Deleted list $id")}));
}

=head2 edit

This one is very similar to list, but it lists list items, not lists.

=cut

sub view :Chained('object') :PathPart('view') :Args(0) {
    my ($self, $c) = @_;
    # get list id
    my $id = $c->stash->{object}->id;
    
    # get list item info from database
    $c->stash(list_items => [$c->model('DB::ListItem')->search(
     { 'list_id' => $id },
     {
       join => {'products' => 'products' },
       '+select' => [ 'products.id', 'products.name', 'products.weight', 'products.cost' ],
       '+as' => [ 'pid', 'name', 'weight', 'cost' ],
     },
     { 'pid' => 'product_id' },
     )->all
    ]);
    $c->stash(list_id => $id);
    
    # use template
    $c->stash(template => 'lists/view.tt2');
}


=head2 create

Rename an existing list with FormHandler

=cut

sub rename : Chained('object') PathPart('rename') Args(0) {
    my ( $self, $c ) = @_;
    
    # $c->set_status_msg("List renamed");
    # did not work
    $c->stash( my_status_msg => "List renamed" );

    return $self->form($c, $c->stash->{object});
}

=head2 create

Use HTML::FormHandler to create a new list

=cut

sub create : Chained('base') : PathPart('create') Args(0) {
    my ($self, $c ) = @_;
    my $list = $c->model('DB::List')->new_result({ user_id => $c->user->id });
    $c->stash( my_status_msg => "List created" );
    return $self->form($c, $list);
}

=head2 form

Process the FormHandler list form
This is a separate function, because it's used by both /create and /edit.

=cut

sub form{
    my ( $self, $c, $list ) = @_;
    my $form = Slists::Form::List->new;
    # Set the template
    $c->stash( template => 'lists/form.tt2', form => $form );
    $form->process(
      item => $list,
      params => $c->req->params,
    );
    return unless $form->validated;
    # Set a status message for the user & return to books list
    return $c->response->redirect($c->uri_for($self->action_for('list'),
        {mid => $c->set_status_msg($c->stash->{my_status_msg})}));
}

=head1 AUTHOR

user,,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
