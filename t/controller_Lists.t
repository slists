use strict;
use warnings;
use Test::More;


use Catalyst::Test 'Slists';
use Slists::Controller::Lists;

ok( request('/lists')->is_success, 'Request should succeed' );
done_testing();
