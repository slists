use strict;
use warnings;

use Slists;

my $app = Slists->apply_default_middlewares(Slists->psgi_app);
$app;

