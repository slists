#!/usr/bin/perl
 
use strict;
use warnings;
 
use Slists::Schema;
 
my $schema = Slists::Schema->connect('dbi:SQLite:slists.db');
 
my @users = $schema->resultset('User')->all;
 
foreach my $user (@users) {
    $user->password('mypass');
    $user->update;
}
